sudo NODE_ENV=production pm2 stop notpunch-api
git pull
yarn install
yarn build
yarn sequelize db:migrate
sudo NODE_ENV=production pm2 start dist/index.js --name notpunch-api
