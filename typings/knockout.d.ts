import 'express';

// Global (non-imported) types
declare global {
  // Custom global variables
  namespace NodeJS {
    interface Global {
      __basedir: string;
    }
  }
  // Extend Request.app
  namespace Express {
    interface Application {
      datasource: any;
    }
  }
  /**
   * Authenticated forum user
   */
  interface User {
    id: number;
    username?: string;
    usergroup: number;
    avatar_url?: string;
    background_url?: string;
    isBanned?: boolean;
    createdAt?: Date;
  }
}

declare module 'express' {
  // Extend the Express request object
  interface Request {
    ipInfo?: string;
    isLoggedIn?: boolean;
    returnEarly?: boolean;
    user?: User;
  }
}

declare module 'redis' {
  // Allow custom properties in the redis client object
  interface RedisClient {
    getAsync: (key: string) => Promise<string>;
    mgetAsync: (keys: Array<string>) => Promise<object>;
  }
}
