'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ban = sequelize.define('Ban', {
    post_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      references: {
        model: "Posts",
        key: "id"
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "Users",
        key: "id"
      }
    },
    banned_by: {
      type: DataTypes.INTEGER,
      references: {
        model: "Users",
        key: "id"
      }
    },
    ban_reason: {
      type: DataTypes.STRING,
    },
    expires_at: {
      type: DataTypes.DATE, field: 'expires_at'
    },
    created_at: {
      type: DataTypes.DATE, field: 'created_at'
    },
    updated_at: {
      type: DataTypes.DATE, field: 'created_at'
    }
  }, {
    timestamps: true
  });
  Ban.associate = function(models) {
    Ban.Post = Ban.belongsTo(models.Post, { as: 'post', foreignKey: 'post_id' });
    Ban.User = Ban.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });
    Ban.BannedBy = Ban.belongsTo(models.User, { as: 'banned_by_user', foreignKey: 'banned_by' });
  };
  Ban.initScopes = () => {}
  return Ban;
};