'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('Tag', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    },
    created_at: {
      type: DataTypes.DATE, field: 'created_at'
    },
    deleted_at: {
      type: DataTypes.DATE, field: 'deleted_at'
    }
  }, {
    timestamps: true
  });
  Tag.initScopes = () => {}
  return Tag;
};