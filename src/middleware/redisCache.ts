import { NextFunction, Request, Response } from "express";
import redis from "../services/redisClient";

import httpStatus from "http-status";
import httpMethods from "../helpers/httpMethods";
import queryComposer from "../helpers/queryComposer";

import { NODE_ENV } from "../../config/server";

export function verifyCache(req: Request, res: Response, next: NextFunction) {
  if (NODE_ENV !== "development" && req.method === httpMethods.get) {
    redis.get(queryComposer.keyCache(req), (err, data) => {
      if (err) throw err;

      if (data != null) {
        const result = JSON.parse(data.toString());

        res.status(httpStatus.OK);
        res.json(result);
      } else {
        next();
      }
    });
  } else {
    next();
  }
};
