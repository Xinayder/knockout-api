import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

router.get('/stats', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

router.get('/stat/today', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

router.get('/stat/posts', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

router.get('/stat/threads', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

router.get('/stat/users', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

router.get('/stat/bans', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;