import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

router.get('/search', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;