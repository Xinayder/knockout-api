import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

// list all users
router.get('/users/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// get a user
router.get('/user/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// delete a user
router.delete('/user/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// ban a user
router.put('/user/:id/ban', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// get a users profile
router.get('/user/:id/profile', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// list a users posts
router.get('/user/:id/posts/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// list a users threads
router.get('/user/:id/threads/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// list a users bans
router.get('/user/:id/bans/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;