import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

// list all subforums
router.get('/subforums', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// list threads within a subforum
router.get('/subforum/:id/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// get a list of ratings enabled for this subforum
router.get('/subforum/:id/ratings', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// get a list of tags used on this subforum
router.get('/subforum/:id/tags', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// create a subforum
router.put('/subforum/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// edit a subforum
router.post('/subforum/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// delete a subforum
router.delete('/subforum/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;