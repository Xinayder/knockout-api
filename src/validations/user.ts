const validateUserFields = (user: User) => {
  if (!user) {
    return false;
  }
  if (
    !user.username ||
    !user.id ||
    !user.avatar_url ||
    !user.usergroup
  ) {
    console.error('user validation failed.');
    console.error('user.username', user.username);
    console.error('user.id', user.id);
    console.error('user.avatar_url', user.avatar_url);
    console.error('user.usergroup', user.usergroup);
    return false;
  }
  if (user.username === '') {
    console.error('user validation failed.')
    console.error('user.username', user.username);
    return false;
  }
  if (user.username.length < 3) {
    console.error('user validation failed.')
    console.error('user.username is ', user.username);
    return false;
  }
  return true;
};

const validateUserAge = async user => {
  // users under a week old should be considered suspicious under certain conditions
  // this validator is used in conjunction with a VPN/datacenter IP check to prevent alts
  const knex = require('../services/knex').knex;
  const result = await knex('Users').where({ id: user.id });
  if (result.length > 0) {
    const now = new Date();
    const createdAt = new Date(result[0].created_at);
    let cutoff = new Date(createdAt);
    cutoff.setDate(cutoff.getDate() + 7);
    return now.getTime() >= cutoff.getTime();
  }
  return false;
};

export { validateUserFields, validateUserAge };
