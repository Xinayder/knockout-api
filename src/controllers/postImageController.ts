/* eslint-disable import/prefer-default-export */

import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { v4 as uuidv4 } from 'uuid';
import { IMAGE_MAX_FILESIZE_BYTES } from 'limits';
import { stripExifTags, imageMetadata } from '../helpers/image';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from '../../config/server';
import errorHandler from '../services/errorHandler';

import { validateUserAge } from '../validations/user';

export const store = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }
    if (!req.file || !req.file.mimetype || !req.file.mimetype.startsWith('image/')) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'Unprocessable entity.' });
      return;
    }

    // prevent new accounts from uploading
    const isNew = !(await validateUserAge(req.user));
    if (isNew) {
      res.status(httpStatus.OK);
      res.json({
        message: 'Your account must be at least a week old to upload images.',
      });
      return;
    }

    const { size } = await imageMetadata(req.file.buffer);

    if (size >= IMAGE_MAX_FILESIZE_BYTES) {
      throw new Error(`Image size must be below ${IMAGE_MAX_FILESIZE_BYTES / 10000} KB`);
    }

    const strippedBuffer = await stripExifTags(req.file.buffer);

    const fileName = `${req.user.id}-${uuidv4()}.${req.file.originalname.split('.').pop()}`;
    const filePath = NODE_ENV === 'production' ? `image/${fileName}` : `avatars/${fileName}`;

    await fileStore.storeBuffer(strippedBuffer, filePath, req.file.mimetype);

    res.status(httpStatus.CREATED);
    res.json({ fileName });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
