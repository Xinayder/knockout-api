/* eslint-disable import/prefer-default-export */
import { Request, Response } from 'express';
import httpStatus from 'http-status';

import ResponseStrategy from '../../helpers/responseStrategy';
import knex from '../../services/knex';
import redis from '../../services/redisClient';
import { getFormattedObjects as getFormattedThreadObjects } from '../../retriever/thread';
import { getFormattedObjects as getFormattedUserObjects } from '../../retriever/user';
import NSFW_TAG_ID from '../../constants/nsfwTagId';
import MIN_ACCT_AGE_SUBFORUM_IDS from '../../constants/minAccountAgeSubforums';
import { validateUserAge } from '../../validations/user';

const GET_ALL_KEY = 'subforums';
const GET_ALL_TTL_SECONDS = 30;

const GET_ALL_LIMITED_KEY = 'subforums-limited';
const GET_ALL_LIMITED_TTL_SECONDS = 30;

const GET_ALL_HIDE_NSFW_KEY = 'subforums-hide-nsfw';
const GETL_ALL_HIDE_NSFW_TTL_SECONDS = 30;

const getSubforums = async (hideNsfw = false, guestOrNewUser = false) => {
  const subforumIdentifier = knex.raw('??', ['Subforums.id']);

  const totalThreads = knex('Threads')
    .count({ all: '*' })
    .where('Threads.subforum_id', subforumIdentifier)
    .whereNull('Threads.deleted_at')
    .as('total_threads');

  const totalPosts = knex('Posts')
    .count({ all: '*' })
    .where('Threads.subforum_id', subforumIdentifier)
    .whereNull('Threads.deleted_at')
    .join('Threads', 'Posts.thread_id', 'Threads.id')
    .as('total_posts');

  const lastPost = knex.raw(
    `(
      select
        p1.id
      from (
        select thread_id,
        max(id) as id
      from Posts
      group by thread_id
      ) as p1
      join Threads
        on p1.thread_id = Threads.id
      left outer join ThreadTags
        on p1.thread_id = ThreadTags.thread_id
      where
        Threads.subforum_id = Subforums.id
        and Threads.deleted_at is null
        ${hideNsfw ? ` and (ThreadTags.tag_id != ${NSFW_TAG_ID} or ThreadTags.tag_id is null)` : ''}
      order by p1.id desc limit 1
    ) as last_post_id`
  );

  const subforums = await knex
    .select(
      'id',
      'name',
      'icon_id',
      'created_at',
      'updated_at',
      'description',
      'icon',
      totalThreads,
      totalPosts,
      lastPost
    )
    .orderBy('Subforums.created_at', 'asc')
    .from('Subforums')
    .where((builder) => {
      if (guestOrNewUser) {
        builder.whereNotIn('id', MIN_ACCT_AGE_SUBFORUM_IDS);
      }
    });

  return subforums;
};

const getLastPosts = async (postIds) => {
  const lastPosts = await knex
    .select(
      'Posts.id as postId',
      'Posts.user_id as postUserId',
      'Posts.created_at as postCreatedAt',
      'Threads.subforum_id as subforumId',
      'Threads.id as threadId'
    )
    .join('Threads', 'Posts.thread_id', 'Threads.id')
    .whereIn('Posts.id', postIds)
    .from('Posts');

  const users = await getFormattedUserObjects(lastPosts.map((lastPost) => lastPost.postUserId));

  const threads = await getFormattedThreadObjects(lastPosts.map((lastPost) => lastPost.threadId));

  const detailedLastPosts = lastPosts.map((lastPost, index) => ({
    id: lastPost.postId,
    createdAt: lastPost.postCreatedAt,
    user: users[index],
    thread: threads[index],
  }));

  return detailedLastPosts.reduce((list, lastPost) => {
    list[lastPost.thread.subforumId] = lastPost;
    return list;
  }, {});
};

// eslint-disable-next-line import/prefer-default-export
export const getAll = async (req: Request, res: Response) => {
  const hideNsfw = req.query.hideNsfw || false;
  const isNew = req.isLoggedIn && !(await validateUserAge(req.user));
  const guestOrNewUser = !req.isLoggedIn || isNew;

  let cacheKey: string;
  let cacheTtl: number;

  if (guestOrNewUser) {
    cacheKey = GET_ALL_LIMITED_KEY;
    cacheTtl = GET_ALL_LIMITED_TTL_SECONDS;
  } else if (hideNsfw) {
    cacheKey = GET_ALL_HIDE_NSFW_KEY;
    cacheTtl = GETL_ALL_HIDE_NSFW_TTL_SECONDS;
  } else {
    cacheKey = GET_ALL_KEY;
    cacheTtl = GET_ALL_TTL_SECONDS;
  }

  const cachedData = await redis.getAsync(cacheKey);

  if (cachedData) {
    res.status(httpStatus.OK);
    return res.json(JSON.parse(cachedData));
  }

  const subforums = await getSubforums(hideNsfw, guestOrNewUser);
  const lastPostIds = subforums.map((subforum) => subforum.last_post_id);

  const lastPosts = await getLastPosts(lastPostIds);

  const output = subforums.map((subforum) => ({
    id: subforum.id,
    name: subforum.name,
    iconId: subforum.icon_id,
    createdAt: subforum.created_at,
    updatedAt: subforum.updated_at,
    description: subforum.description,
    icon: subforum.icon,
    totalThreads: subforum.total_threads,
    totalPosts: subforum.total_posts,
    lastPostId: subforum.last_post_id,
    lastPost: lastPosts[subforum.id],
  }));

  const responseBody = { list: output };

  redis.setex(cacheKey, cacheTtl, JSON.stringify(responseBody));

  return ResponseStrategy.send(res, responseBody);
};
