import { Request, Response } from 'express';
import Knex from 'knex';
import { MODERATOR_GROUPS } from '../constants/userGroups';
import ResponseStrategy from '../helpers/responseStrategy';
import User, { getFormattedObjects } from '../retriever/user';
import knex from '../services/knex';

// eslint-disable-next-line import/prefer-default-export
export const search = async (req: Request, res: Response) => {
  const offset = req.params.page ? Number(req.params.page) * 40 - 40 : 0;
  const filter = req.query.filter || null;
  const modTable = req.query.modTable && MODERATOR_GROUPS.includes(req.user.usergroup);

  const setBuilderParams = (builder: Knex.QueryBuilder) => {
    if (!filter) return;
    builder.where('u.username', 'like', `${filter}%`);
    if (!modTable) builder.andWhereNot('u.id', req.user.id);
  };

  const allUsers = await knex
    .from('Users as u')
    .count('u.id as count')
    .where(setBuilderParams)
    .first();

  const query = await knex
    .from('Users as u')
    .select('u.id')
    .where(setBuilderParams)
    .orderBy('u.id', 'desc')
    .limit(40)
    .offset(offset);

  const userIds = query.map((user) => user.id);

  const users = await getFormattedObjects(userIds, modTable ? [] : [User.HIDE_BANNED]);
  return ResponseStrategy.send(res, {
    totalUsers: allUsers.count,
    currentPage: Number(req.params.page) || 1,
    users,
  });
};
