import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import redis from '../services/redisClient';

import composer from '../helpers/queryComposer';
import responseStrategy from '../helpers/responseStrategy';
import Pagination from '../helpers/PaginationBuilder';
import { isProxy } from '../helpers/proxyDetect';
import postMentionFinder from '../helpers/postMentionFinder';
import { getCountryName } from '../helpers/geoipLookup';
import { validateAppName } from '../validations/post';
import { MODERATOR_GROUPS } from '../constants/userGroups';
import { postsPerPage } from '../constants/pagination';
import { invalidateObject as invalidatePost } from '../retriever/post';
import MIN_ACCT_AGE_SUBFORUM_IDS from '../constants/minAccountAgeSubforums';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import { invalidateObject, invalidateObjectPosts } from '../retriever/thread';
import errorHandler from '../services/errorHandler';
import { THREAD_POST_LIMIT } from '../constants/limits';

const datasource = require('../models/datasource');
const { getFormattedObject } = require('../retriever/thread');

const eventLogController = require('./eventLogController');

const { validateThreadStatus, validatePostLength } = require('../validations/post');
const { validateUserFields, validateUserAge } = require('../validations/user');

const { createMention } = require('./mentionsController');

const { Post, Rating, IpAddress } = datasource().models;

export const index = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  options.distinct = true;
  options.col = 'Post.id';

  try {
    const result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    responseStrategy.send(res, pagResult, options);
  } catch (error) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};
exports.show = (req, res) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  scope
    .findOne(options)
    .then((result) => {
      redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

      responseStrategy.send(res, result, options);
    })
    .catch((err) => {
      errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, err, res);
    });
};

export const store = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on postController.store');
    }
    if (req.user.isBanned) {
      throw new Error('User is banned');
    }

    const userIsMod = MODERATOR_GROUPS.includes(req.user.usergroup);

    // validations for a valid post
    if (
      !userIsMod &&
      (!(await validateThreadStatus(req.body.thread_id)) || !validatePostLength(req.body.content))
    ) {
      throw new Error('Failed validations.');
    }

    if (!validateUserFields(req.user)) {
      throw new Error('Invalid user.');
    }

    // client name logic
    let appName: string | null;

    if (req.body.appName) {
      appName = validateAppName(req.body.appName) ? req.body.appName : null;
    } else {
      appName = null;
    }

    // is the user under a week old?
    const isNew = !(await validateUserAge(req.user));
    if (isNew) {
      // are they using a VPN or do they look like they're in a datacenter?
      const ip = req.ipInfo;
      if (isProxy(ip)) {
        console.log('VPN detected', ip);
        throw new Error('You appear to be using a proxy/VPN.');
      }

      // is this user trying to post in a restricted subforum?
      const thread = await getFormattedObject(req.body.thread_id);
      const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);
      if (raidModeEnabled || MIN_ACCT_AGE_SUBFORUM_IDS.includes(thread.subforumId)) {
        return res.status(httpStatus.FORBIDDEN).json({
          error: 'Your account is too new to post in this subforum.',
        });
      }
    }

    let result;
    let threadPostCount;
    let post;

    // Start transaction here so we can avoid race conditions with posts
    await knex.transaction(async (trx) => {
      // Get thread post count
      // Needed to set the thread post number
      // And check for auto-lock
      threadPostCount = await trx('Posts')
        .count('id as count')
        .where({ thread_id: req.body.thread_id })
        .then((output) => output[0].count);

      //
      // automerge logic
      // if the latest post in a thread is by this
      // user, merge posts instead of creating a new one
      //
      const lastPostQuery = await trx
        .from('Posts as po')
        .select('po.id', 'po.content', 'po.user_id')
        .where(
          knex.raw('po.created_at >= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND po.thread_id = ?', [
            req.body.thread_id,
          ])
        )
        .limit(1)
        .orderByRaw('po.id DESC');

      let lastPostId = null;
      if (lastPostQuery.length > 0 && lastPostQuery[0].user_id === req.user.id) {
        // automerge
        lastPostId = lastPostQuery[0].id;
        const postContent = lastPostQuery[0].content;
        const newContent = req.body.content;
        const resultingContent = `${postContent}

[b]Edited:[/b]

${newContent}`;

        if (!validatePostLength(resultingContent)) {
          throw new Error('Failed validations.');
        }

        await trx('Posts').where({ id: lastPostId }).update({
          updated_at: new Date(),
          content: resultingContent,
          app_name: appName,
        });
        invalidatePost(lastPostId);

        post = await trx('Posts').select('id', 'user_id', 'thread_id').where({ id: lastPostId });
        // eslint-disable-next-line prefer-destructuring
        post = post[0];
      } else {
        // country information logic
        const countryName = req.body.displayCountryInfo ? getCountryName(req.ipInfo) : null;

        // new thread post number
        const threadPostNumber = threadPostCount + 1;
        threadPostCount += 1;

        // new post
        post = await Post.create({
          ...req.body,
          content:
            typeof req.body.content === 'string'
              ? req.body.content
              : JSON.stringify(req.body.content),
          user_id: req.user.id,
          country_name: countryName,
          app_name: appName,
          thread_post_number: threadPostNumber,
        });

        // new ip address
        IpAddress.create({
          ip_address: req.ipInfo,
          user_id: req.user.id,
          post_id: post.id,
        });
        lastPostId = post.id;
      }

      const scope = composer.scope(req, Post);
      result = await scope.findOne({ where: { id: lastPostId } });
    });

    //
    // if a thread has reached the post limit,
    // it should be locked automatically.
    //
    const lockedStatus = threadPostCount >= THREAD_POST_LIMIT;

    if (lockedStatus) {
      eventLogController.threadAutoLock({
        threadId: req.body.thread_id,
      });
    }

    //
    // update Thread updated_at
    //
    await knex('Threads')
      .where({ id: req.body.thread_id })
      .update('updated_at', new Date())
      .update('locked', lockedStatus ? 1 : undefined);

    //
    // invalidate thread object cache
    //
    invalidateObjectPosts(req.body.thread_id);
    if (lockedStatus) {
      invalidateObject(req.body.thread_id);
    }

    //
    // mentions logic
    //
    if (post && post.id) {
      // go through the entire value again and look for mentions :/
      const mentions = postMentionFinder(req.body.content);

      if (mentions && mentions.length > 0) {
        const thread = await knex('Threads').select('title').where('id', req.body.thread_id);

        // build message notification
        const content = JSON.stringify([
          `💬 ${req.user.username} mentioned you in "${thread[0].title}".`,
        ]);

        // construct the page number
        const page = Math.ceil(threadPostCount / postsPerPage);

        const mentionQueries = [];

        mentions.forEach((userId) => {
          if (userId !== req.user.id) {
            mentionQueries.push(
              createMention({
                isLoggedIn: req.isLoggedIn,
                postId: post.id,
                mentionsUser: userId,
                content,
                inactive: false,
                threadId: req.body.thread_id,
                threadTitle: thread[0].title,
                page,
                userId: req.user.id,
              })
            );
          }
        });
        await Promise.all(mentionQueries);
      }
    }

    if (req.returnEarly === true) {
      return undefined;
    }
    res.status(httpStatus.CREATED);
    return res.json(result);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on postController.store');
    }

    const userIsMod = MODERATOR_GROUPS.includes(req.user.usergroup);

    // validations for a valid post
    if (!validatePostLength(req.body.content)) {
      throw new Error('Failed validations.');
    }
    if (!validateUserFields(req.user)) {
      throw new Error('Invalid user.');
    }
    if (req.user.isBanned) {
      throw new Error('User is banned.');
    }

    const targetUser = userIsMod ? {} : { user_id: req.user.id };

    const contentToDb =
      typeof req.body.content === 'string' ? req.body.content : JSON.stringify(req.body.content);

    // client name logic
    let appName: string | null;

    if (req.body.appName) {
      appName = validateAppName(req.body.appName) ? req.body.appName : null;
    } else {
      appName = null;
    }

    await knex('Posts')
      .where({ id: req.body.id, ...targetUser })
      .update({
        content: contentToDb,
        updated_at: knex.fn.now(),
        app_name: appName,
      });

    // create new IP corresponding to this post
    IpAddress.create({
      ip_address: req.ipInfo,
      post_id: req.body.id,
      user_id: req.user.id,
    });

    invalidatePost(req.body.id);
    res.status(httpStatus.CREATED);
    res.json({ message: 'Post updated' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const destroy = async (req: Request, res: Response) => {
  Post.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};

exports.count = (req, res) => {
  const options = composer.onlyQuery(req);

  Post.count(options)
    .then((result) => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch((err) => {
      errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, err, res);
    });
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  try {
    const queryResults = await knex
      .from('Posts as po')
      .select(
        'po.id as postId',
        'po.content as postContent',
        'po.user_id as postUserId',
        'po.created_at as postCreatedAt',
        'po.updated_at as postUpdatedAt',
        'po.thread_id as postThreadId',
        'us.username as userUsername',
        'us.usergroup as userUsergroup',
        'us.avatar_url as userAvatar_url',
        'us.background_url as userBackgroundUrl',
        'us.created_at as userCreated_at',
        knex.raw(
          '(select count (*) from Bans where Bans.user_id = po.user_id AND expires_at > NOW()) as postUserBans'
        )
      )
      .leftJoin('Ratings as rat', 'po.id', 'rat.post_id')
      .join('Users as us', 'us.id', 'po.user_id')
      .where(knex.raw('po.id = ?', req.params.id))
      .limit(1);

    if (queryResults.length === 0) {
      return responseStrategy.send(res, null);
    }

    const postWithUser = queryResults[0];

    const ratings = await Rating.getRatingsForPostsWithUsers([postWithUser.postId]);

    const response = {
      id: postWithUser.postId,
      content: postWithUser.postContent,
      createdAt: postWithUser.postCreatedAt,
      updatedAt: postWithUser.postUpdatedAt,
      user: {
        id: postWithUser.postUserId,
        username: postWithUser.userUsername,
        usergroup: postWithUser.userUsergroup,
        avatar_url: postWithUser.userAvatar_url,
        backgroundUrl: postWithUser.userBackgroundUrl,
        createdAt: postWithUser.userCreated_at,
        isBanned: postWithUser.postUserBans > 0,
      },
      ratings: ratings[postWithUser.postId],
    };

    return responseStrategy.send(res, response);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};
