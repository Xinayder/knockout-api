import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import Conversation, { getFormattedObject, getFormattedObjects } from '../retriever/conversation';
import errorHandler from '../services/errorHandler';

export const isUserInConversation = async (conversationId: number, userId: number) => {
  const query = await knex('ConversationUsers as cu')
    .where('cu.user_id', userId)
    .andWhere('cu.conversation_id', conversationId)
    .limit(1);

  return query.length > 0;
};

/**
 * Shows a conversation
 */
export const show = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json([]);
      return;
    }

    // requesting user must be part of the
    // conversation requested
    const userInConversation = await isUserInConversation(req.params.id, req.user.id);

    if (!userInConversation) {
      res.status(httpStatus.FORBIDDEN);
      res.json([]);
      return;
    }

    const results = await getFormattedObject(req.params.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

const getConversations = async (userId: number) => {
  const query = await knex('Conversations as c')
    .select('c.id')
    .join('ConversationUsers as cu', 'cu.conversation_id', 'c.id')
    .where('cu.user_id', [userId])
    .orderBy('c.updated_at', 'desc');

  const results = await getFormattedObjects(
    query.map((item) => item.id),
    [Conversation.RETRIEVE_SHALLOW]
  );

  return results;
};

export const index = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json([]);
      return;
    }

    const results = await getConversations(req.user.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
