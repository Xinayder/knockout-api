import { Request, Response } from 'express';
import responseStrategy from '../../helpers/responseStrategy';
import { getFormattedObject } from '../../retriever/report';

// eslint-disable-next-line import/prefer-default-export
export const get = async (req: Request, res: Response) => {
  const report = await getFormattedObject(req.params.id);
  return responseStrategy.send(res, report);
};
