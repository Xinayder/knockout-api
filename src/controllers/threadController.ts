import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { POSTS_PER_PAGE } from '../../config/server';
import Pagination from '../helpers/PaginationBuilder';
import composer from '../helpers/queryComposer';
import ResponseStrategy from '../helpers/responseStrategy';
import datasource from '../models/datasource';
import ThreadRetriever, {
  getFormattedObject,
  getFormattedObjects,
  invalidateObject,
} from '../retriever/thread';
import knex from '../services/knex';
import redis from '../services/redisClient';
import { validatePostLength } from '../validations/post';
import { validateThreadTitleLength, validateThreadTagCount } from '../validations/thread';
import { validateUserFields, validateUserAge } from '../validations/user';
import { isProxy } from '../helpers/proxyDetect';
import * as eventLogController from './eventLogController';
import * as postController from './postController';
import { GOLD_MEMBER_GROUPS, MODERATOR_GROUPS } from '../constants/userGroups';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import MIN_ACCT_AGE_SUBFORUM_IDS from '../constants/minAccountAgeSubforums';
import errorHandler from '../services/errorHandler';
import { getFormattedObjects as getPostObjects } from '../retriever/post';

const { Thread } = datasource().models;

const POPULAR_THREADS_KEY = 'popular-threads';
const LATEST_THREADS_KEY = 'latest-threads';

export const index = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Thread);
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = 'Thread.id';

  try {
    const result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    ResponseStrategy.send(res, pagResult, options);
  } catch (error) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const popular = async (req: Request, res: Response) => {
  const threadsCache = await redis.getAsync(POPULAR_THREADS_KEY);

  if (threadsCache) {
    return ResponseStrategy.send(res, { list: JSON.parse(threadsCache) });
  }

  const query = await knex
    .from('Posts as po')
    .select('th.id', knex.raw('count(po.id) as replyCount'))
    .join('Threads as th', 'po.thread_id', 'th.id')
    .whereRaw(
      'po.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY) AND locked = 0 AND th.deleted_at IS NULL'
    )
    .groupBy('th.id')
    .orderByRaw('replyCount DESC')
    .limit(20);

  const threadIds = query.map((item) => item.id);

  const threads = await getFormattedObjects(threadIds, [
    ThreadRetriever.RETRIEVE_SHALLOW,
    ThreadRetriever.INCLUDE_USER,
    ThreadRetriever.INCLUDE_TAGS,
    ThreadRetriever.INCLUDE_RECENT_POSTS,
  ]);

  redis.setex(POPULAR_THREADS_KEY, 60, JSON.stringify(threads));

  return ResponseStrategy.send(res, { list: threads });
};

export const latest = async (req: Request, res: Response) => {
  const threadsCache = await redis.getAsync(LATEST_THREADS_KEY);

  if (threadsCache) {
    return ResponseStrategy.send(res, { list: JSON.parse(threadsCache) });
  }

  const query = await knex
    .from('Threads as th')
    .select('th.id')
    .whereRaw('locked = 0 AND th.deleted_at IS NULL')
    .limit(20)
    .orderByRaw('th.created_at DESC');

  const threadIds = query.map((item) => item.id);

  const threads = await getFormattedObjects(threadIds, []);

  redis.set(LATEST_THREADS_KEY, JSON.stringify(threads));

  return ResponseStrategy.send(res, { list: threads });
};

export const show = async (req: Request, res: Response) => {
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = 'Thread.id';
  options.parameters = [req.query.postPage, req.params.id];

  const scope = composer.scope(req, Thread, options);

  try {
    const result = await scope.findOne(options);

    redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

    ResponseStrategy.send(res, result, options);
  } catch (error) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const store = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Thread);

  if (req.user.isBanned) {
    throw new Error('User is banned');
  }

  if (
    !req.body.title ||
    !validateThreadTitleLength(req.body.title) ||
    !validatePostLength(req.body.content) ||
    !validateThreadTagCount(req.body.tagIds)
  ) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'Invalid content' });
    return;
  }

  if (!validateUserFields(req.user)) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({ error: 'Invalid user' });
    return;
  }

  // is the user under a week old?
  const isNew = !(await validateUserAge(req.user));
  if (isNew) {
    // are they using a VPN or do they look like they're in a datacenter?
    const ip = req.ipInfo;
    if (isProxy(ip)) {
      console.log('VPN detected', ip);
      throw new Error('You appear to be using a proxy/VPN.');
    }

    const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);

    if (raidModeEnabled || MIN_ACCT_AGE_SUBFORUM_IDS.includes(Number(req.body.subforum_id))) {
      res
        .status(httpStatus.FORBIDDEN)
        .json({ error: 'Your account is too new to post in this subforum.' });
      return;
    }
  }

  // validations for a valid post
  if (!validatePostLength(req.body.content)) {
    throw new Error('Failed validations.');
  }

  try {
    // check if user is a gold member. if he's not and sent a bg url,
    // boot him.
    const allowedUsergroups = GOLD_MEMBER_GROUPS;
    if (!allowedUsergroups.includes(req.user.usergroup) && req.body.backgroundUrl) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      throw new Error('Not authorized.');
    }

    const thread = await Thread.create({
      ...req.body,
      user_id: req.user.id,
      background_url: req.body.backgroundUrl,
      background_type: req.body.backgroundType,
    });

    postController.store(
      { ...req, returnEarly: true, body: { ...req.body, thread_id: thread.id } } as Request,
      res
    );

    // Insert thread tags
    if (req.body.tagIds && req.body.tagIds.length > 0) {
      await knex('ThreadTags').insert(
        req.body.tagIds.map((tagId) => ({
          tag_id: tagId,
          thread_id: thread.id,
        }))
      );
    }

    const result = await scope.findOne({ where: { id: thread.id } });

    redis.del(LATEST_THREADS_KEY);

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const update = async (req: Request, res: Response) => {
  const allowedUsergroups = MODERATOR_GROUPS;

  // this is a logged in only action
  if (!req.isLoggedIn || !req.user || req.user.isBanned) {
    res.status(httpStatus.FORBIDDEN);
    res.json({ error: 'Forbidden' });
    return;
  }

  const scope = composer.scope(req, Thread);

  try {
    const thread = await Thread.findOne({ where: { id: req.body.id } });
    const oldTitle = thread.title;

    // if this user is not a mod and not the author of the thread, he cant edit it
    if (!allowedUsergroups.includes(req.user.usergroup) && thread.user_id !== req.user.id) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    interface UpdatePayloadInterface {
      title?: string;
      background_url?: string;
      background_type?: string;
      subforum_id?: number;
    }

    const updatePayload: UpdatePayloadInterface = {};

    if (req.body.title) {
      if (!validateThreadTitleLength(req.body.title)) {
        res.status(httpStatus.FORBIDDEN);
        res.json({ error: 'Invalid thread title' });
        return;
      }

      updatePayload.title = req.body.title;
    }

    if (req.body.subforum_id && allowedUsergroups.includes(req.user.usergroup)) {
      updatePayload.subforum_id = req.body.subforum_id;
    }

    if (req.body.backgroundUrl || req.body.backgroundType) {
      // check if user is a gold member. if he's not and sent a bg url/type, boot him.
      if (!GOLD_MEMBER_GROUPS.includes(req.user.usergroup)) {
        res.status(httpStatus.FORBIDDEN);
        res.json({ error: 'Forbidden' });
        throw new Error('Not authorized.');
      }

      if (req.body.backgroundUrl) {
        updatePayload.background_url = req.body.backgroundUrl;
      }

      if (req.body.backgroundType) {
        updatePayload.background_type = req.body.backgroundType;
      }
    }

    if (thread.locked === false) {
      await knex('Threads').where({ id: req.body.id }).update(updatePayload);
    } else {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: "You can't do that, dummy" });
      return;
    }

    await invalidateObject(req.body.id);

    const result = await scope.findOne({ where: { id: req.body.id } });

    if (updatePayload.title) {
      eventLogController.threadUpdateEvent({
        username: req.user.username,
        oldTitle,
        newTitle: result.title,
        threadId: req.body.id,
        userId: req.user.id,
        body: req.body,
      });
    }

    if (updatePayload.background_url) {
      eventLogController.threadBackgroundUpdateEvent({
        username: req.user.username,
        threadTitle: thread.title,
        threadId: req.body.id,
        userId: req.user.id,
      });
    }

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};

export const destroy = async (req: Request, res: Response) => {
  Thread.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};

export const count = (req: Request, res: Response) => {
  const options = composer.onlyQuery(req);

  Thread.count(options)
    .then((result) => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch((err) => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  // if no page parameter is provided, default to page 1
  const pageParam: number = Number(req.params.page) || 1;

  try {
    // get total posts for this thread
    const postIds = await knex
      .from('Posts as po')
      .select('po.id as id')
      .where('po.thread_id', req.params.id)
      .limit(Number(POSTS_PER_PAGE))
      .offset(Number(POSTS_PER_PAGE) * (pageParam - 1));

    const currentUserGroup = req.user && req.user.usergroup ? req.user.usergroup : 0;
    const userIsMod = MODERATOR_GROUPS.includes(currentUserGroup);

    const flags = [
      ThreadRetriever.RETRIEVE_SHALLOW,
      ThreadRetriever.INCLUDE_LAST_POST,
      ThreadRetriever.INCLUDE_USER,
      ThreadRetriever.INCLUDE_TAGS,
      ThreadRetriever.INCLUDE_SUBFORUM,
    ];

    if (!userIsMod) {
      flags.push(ThreadRetriever.HIDE_DELETED);
    }

    const thread = await getFormattedObject(req.params.id, flags);

    const isNew = req.isLoggedIn && !(await validateUserAge(req.user));
    const guestOrNewUser = !req.isLoggedIn || isNew;

    if (guestOrNewUser && MIN_ACCT_AGE_SUBFORUM_IDS.includes(thread.subforumId)) {
      res.status(httpStatus.FORBIDDEN);
      return res.json({});
    }

    if (postIds.length > 0) {
      const posts = await getPostObjects(
        postIds.map((post) => post.id),
        []
      );

      const response: any = {
        ...thread,
        totalPosts: thread.postCount,
        currentPage: pageParam,
        threadBackgroundUrl: thread.backgroundUrl,
        threadBackgroundType: thread.backgroundType,
        posts,
      };

      if (req.isLoggedIn) {
        // get read threads
        const unreadPosts = await knex
          .select('thread_id', 'last_seen as lastSeen')
          .first()
          .from('ReadThreads')
          .whereRaw('ReadThreads.thread_id = ? AND ReadThreads.user_id = ?', [
            req.params.id,
            req.user.id,
          ]);

        if (unreadPosts) {
          response.readThreadLastSeen = unreadPosts.lastSeen;
        }

        // get subscriptions (alerts)
        const subscriptions = await knex
          .select('thread_id', 'last_seen as lastSeen', 'last_post_number as lastPostNumber')
          .first()
          .from('Alerts')
          .whereRaw('Alerts.thread_id = ? AND Alerts.user_id = ?', [req.params.id, req.user.id]);

        response.isSubscribedTo = subscriptions && subscriptions.thread_id;
        if (response.isSubscribedTo) {
          response.subscriptionLastSeen = subscriptions.lastSeen;
          response.subscriptionLastPostNumber = subscriptions.lastPostNumber;
        }
      }

      return ResponseStrategy.send(res, response);
    }

    const response = {
      ...thread,
      currentPage: pageParam,
      totalPosts: thread.postCount,
      posts: [],
    };

    return ResponseStrategy.send(res, response);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const updateTags = async (req: Request, res: Response) => {
  if (req.user.isBanned) {
    throw new Error('User is banned');
  }

  if (!validateUserFields(req.user)) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({ error: 'Invalid user' });
    return undefined;
  }

  // at this time only mods can edit an existing thread's tags
  if (!MODERATOR_GROUPS.includes(req.user.usergroup)) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({ error: 'Invalid user' });
    return undefined;
  }

  if (!req.body.threadId || !req.body.tags || req.body.tags.length > 3) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    return res.json({ error: 'Invalid content' });
  }

  // delete all existing tags for that thread
  await knex('ThreadTags').delete().where({
    thread_id: req.body.threadId,
  });

  if (req.body.tags.length > 0) {
    await knex('ThreadTags').insert(
      req.body.tags.map((tag) => ({
        tag_id: tag,
        thread_id: req.body.threadId,
      }))
    );
  }

  await invalidateObject(req.body.threadId);

  res.status(httpStatus.OK);
  return res.json({ message: 'Tags updated.' });
};
