import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { getFormattedObjects } from '../retriever/user';
import errorHandler from '../services/errorHandler';
import knex from '../services/knex';

export const createMention = async ({
  isLoggedIn,
  postId,
  mentionsUser,
  content,
  inactive,
  threadId,
  threadTitle,
  page,
  userId,
}) => {
  try {
    if (!isLoggedIn) {
      throw new Error('Bad user login on mentionsController.store');
    }

    const result = await knex('Mentions').insert({
      post_id: postId,
      mentions_user: mentionsUser,
      content,
      inactive,
      thread_id: threadId,
      thread_title: threadTitle,
      page,
      mentioned_by: userId,
    });

    return result;
  } catch (exception) {
    return new Error(exception);
  }
};

export const markAsRead = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on mentionsController.markAsRead');
    }
    if (!req.body.postIds) {
      throw new Error('No postIds supplied.');
    }

    const postIds = Array.isArray(req.body.postIds) ? req.body.postIds : [req.body.postIds];

    await knex('Mentions')
      .update({ inactive: true })
      .where({ mentions_user: req.user.id })
      .whereIn('post_id', postIds);

    res.status(httpStatus.OK);
    res.json({ message: 'Posts marked as read!' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getMentionsQuery = async (userId: number) => {
  const mentions: any[] = await knex('Mentions')
    .select(
      'Mentions.id as mentionId',
      'Mentions.post_id as postId',
      'Mentions.content',
      'Mentions.created_at as createdAt',
      'Mentions.thread_id as threadId',
      'Mentions.page as threadPage',
      'Mentions.thread_title as threadTitle',
      'Mentions.mentioned_by as mentionedBy'
    )
    .where({ mentions_user: userId, inactive: false })
    .orWhere({ mentions_user: userId, inactive: null })
    .limit(20);

  const users = Array.from(new Set(mentions.map((result) => result.mentionedBy)));
  const userMap = (await getFormattedObjects(users)).reduce((map, user) => {
    map[user.id] = user;
    return map;
  }, {});
  const results = mentions.map((mention) => ({
    ...mention,
    mentionedBy: userMap[mention.mentionedBy],
  }));

  return results;
};

export const index = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json([]);
      return;
    }

    const results = await getMentionsQuery(req.user.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
