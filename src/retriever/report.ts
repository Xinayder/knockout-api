import AbstractRetriever from './abstractRetriever';
import User from './user';
import { getFormattedObjects as getFormattedPosts } from './post';
import knex from '../services/knex';

export default class Report extends AbstractRetriever {
  private cachePrefix: string = 'report';

  private static async getReports(ids: Array<number>) {
    const results = await knex('Reports')
      .select(
        'id',
        'post_id as postId',
        'reported_by as reportedBy',
        'report_reason as reportReason',
        'solved_by as solvedBy',
        'created_at as createdAt',
        'updated_at as updatedAt'
      )
      .whereIn('id', ids);

    return results.reduce((list, report) => {
      list[report.id] = report;
      return list;
    }, {});
  }

  private static async getUsers(reports: Array<any>) {
    const userIds = reports.reduce((list, report) => {
      list.add(report.reportedBy);
      list.add(report.solvedBy);
      return list;
    }, new Set([]));

    const userRetriever = new User(Array.from(userIds), []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private static async getPosts(reports: Array<any>) {
    const postIds = reports
      .map((report) => report.post)
      .filter((elem, index, self) => index === self.indexOf(elem));

    const rawPosts = await getFormattedPosts(postIds);
    return rawPosts.reduce((list, post) => {
      list[post.id] = post;
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.id,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      reason: data.reportReason,
      post: data.postId,
      reportedBy: data.reportedBy,
      solvedBy: data.solvedBy,
    };
  }

  async get() {
    // grab canonical data
    const cachedReports = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedReports);
    const uncachedReports = await Report.getReports(uncachedIds);
    const reports = this.ids.map((id, index) => {
      if (cachedReports[index] !== null) return cachedReports[index];
      return Report.format(uncachedReports[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      reports.map(async (report) => {
        await this.cacheSet(this.cachePrefix, report.id, report);
      });
    }

    // grab data from related caches
    const users = await Report.getUsers(reports);
    const posts = await Report.getPosts(reports);

    // merge related data in
    return reports.map((report) => {
      report.reportedBy = users[report.reportedBy] || null;
      report.solvedBy = users[report.solvedBy] || null;
      report.post = posts[report.post] || null;
      return report;
    });
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const ReportRetriever = new Report(ids, []);
  await ReportRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const ReportRetriever = new Report([id], []);
  await ReportRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>) => {
  const ReportRetriever = new Report(ids, []);
  return ReportRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const ReportRetriever = new Report([id], []);
  const reports = await ReportRetriever.get();
  return reports[0];
};
