/* eslint-disable import/no-cycle */
import AbstractRetriever from './abstractRetriever';
import User from './user';
import Post from './post';
import knex from '../services/knex';
import Postrating from './postRating';
import Subforum from './subforum';
import NSFW_TAG_ID from '../constants/nsfwTagId';

export default class Thread extends AbstractRetriever {
  private cachePrefix: string = 'thread';

  private postCachePrefix: string = 'threadPosts';

  private recentPostCachePrefix: string = 'threadRecentPosts';

  public static INCLUDE_SUBFORUM = 1;

  public static RETRIEVE_SHALLOW = 2;

  public static INCLUDE_USER = 3;

  public static INCLUDE_LAST_POST = 4;

  public static EXCLUDE_READ_THREADS = 5;

  public static HIDE_DELETED = 6;

  public static INCLUDE_RECENT_POSTS = 7;

  public static INCLUDE_TAGS = 8;

  public static HIDE_NSFW = 9;

  private async getThreads(ids: Array<number>) {
    const threadQuery = await knex
      .from('Threads as th')
      .select(
        'th.id as threadId',
        'th.title as threadTitle',
        'th.icon_id as threadIconId',
        'th.user_id as threadUserId',
        'th.subforum_id as threadSubforumId',
        'th.created_at as threadCreatedAt',
        'th.updated_at as threadUpdatedAt',
        'th.deleted_at as threadDeletedAt',
        'th.locked as threadLocked',
        'th.pinned as threadPinned',
        'th.background_url as threadBackgroundUrl',
        'th.background_type as threadBackgroundType'
      )
      .whereIn('th.id', ids)
      .where((builder) => {
        if (this.hasFlag(Thread.HIDE_DELETED)) {
          builder.whereNull('deleted_at');
        }
      });

    return threadQuery.reduce((list, thread) => {
      list[thread.threadId] = {
        ...thread,
      };
      return list;
    }, {});
  }

  private async getThreadTags(ids: Array<number>) {
    if (this.hasFlag(Thread.RETRIEVE_SHALLOW) && !this.hasFlag(Thread.INCLUDE_TAGS)) return {};
    const threadQuery = await knex
      .from('ThreadTags as tht')
      .select('tht.thread_id as threadId', 'tht.tag_id as tagId', 't.name as tagName')
      .leftJoin('Tags as t', 'tht.tag_id', 't.id')
      .whereIn('tht.thread_id', ids)
      .groupBy('threadId', 'tagId');

    // make into an array of tag {id: name} objects
    return threadQuery.reduce(
      (list, row) => ({
        ...list,
        [row.threadId]: list[row.threadId]
          ? [...list[row.threadId], { [row.tagId]: row.tagName }]
          : [{ [row.tagId]: row.tagName }],
      }),
      {}
    );
  }

  private async getUsers(threads: Array<any>) {
    if (this.hasFlag(Thread.RETRIEVE_SHALLOW) && !this.hasFlag(Thread.INCLUDE_USER)) return {};
    const userIds = threads.map((thread) => thread.user);
    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private async getFirstPostRatings(threads: Array<any>) {
    if (this.hasFlag(Thread.RETRIEVE_SHALLOW)) return {};
    const ids = threads.map((thread) => thread.id);

    const firstPosts = await knex('Posts')
      .select('Posts.id', 'Posts.thread_id')
      .whereIn('Posts.thread_id', ids)
      .orderBy('Posts.id', 'asc')
      .groupBy('Posts.thread_id');

    const postIds: number[] = firstPosts.map((el) => el.id);

    const postThreadDict = firstPosts.reduce((acc, val) => {
      acc[val.id] = val.thread_id;
      return acc;
    }, {});

    const PostRatingRetriever = new Postrating(postIds);

    const firstPostRatings = await PostRatingRetriever.get();

    const threadTopRatings = {};

    firstPostRatings.forEach((thread) => {
      for (let j = 0; j < thread.length; j++) {
        const rating = thread[j];
        delete rating.users;
        const threadId = postThreadDict[rating.id];

        if (
          (!threadTopRatings[threadId] || threadTopRatings[threadId].count < rating.count) &&
          rating.count >= 10
        ) {
          threadTopRatings[threadId] = rating;
        }
      }
    });

    return threadTopRatings;
  }

  private async getSubforums(threads: Array<any>) {
    if (!this.hasFlag(Thread.INCLUDE_SUBFORUM)) return [];
    const subforumIds = threads.map((thread) => thread.subforumId).filter((id) => id !== null);

    const subforumRetriever = new Subforum(subforumIds, []);
    const rawSubforums = await subforumRetriever.get();
    return rawSubforums.reduce((list, subforum) => {
      list[subforum.id] = subforum;
      return list;
    }, {});
  }

  private async getLastPosts() {
    if (this.hasFlag(Thread.RETRIEVE_SHALLOW) && !this.hasFlag(Thread.INCLUDE_LAST_POST)) return {};
    const cachedLastPosts = await this.cacheGet(this.postCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedLastPosts);

    const postCounts = await knex
      .from('Posts as p')
      .select('p.thread_id as id')
      .count('id as postCount')
      .whereIn('p.thread_id', uncachedIds)
      .groupBy('p.thread_id');

    const queries = postCounts.map((thread) =>
      knex
        .select('Posts.thread_id as threadId', 'Posts.id as postId')
        .first()
        .from('Posts')
        .where('Posts.thread_id', thread.id)
        .where('Posts.thread_post_number', thread.postCount)
    );

    const lastPosts: Array<any> = await Promise.all(queries);
    const postIds = lastPosts.map((post) => (post ? post['postId'] : null));
    const postRetriever = new Post(postIds, []);
    const rawPosts = await postRetriever.get();

    const countMap = postCounts.reduce((dict, count) => {
      dict[count.id] = count.postCount;
      return dict;
    }, {});

    const result = lastPosts.reduce((list, post, index) => {
      if (post) {
        list[post.threadId] = { postCount: countMap[post.threadId], lastPost: rawPosts[index] };
        this.cacheSet(this.postCachePrefix, post.threadId, list[post.threadId]);
      }
      return list;
    }, {});

    this.ids.forEach((id, index) => {
      if (cachedLastPosts[index] !== null) result[id] = cachedLastPosts[index];
    });
    return result;
  }

  private async getRecentPosts() {
    if (!this.hasFlag(Thread.INCLUDE_RECENT_POSTS)) return {};
    const cachedRecentPosts = await this.cacheGet(this.recentPostCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedRecentPosts);

    const postCounts = await knex
      .from('Posts as p')
      .select('p.thread_id as id')
      .count('id as threadRecentPostCount')
      .whereIn('p.thread_id', uncachedIds)
      .where('p.created_at', '>=', 'DATE_SUB(NOW(), INTERVAL 1 DAY))')
      .groupBy('p.thread_id');

    const result = postCounts.reduce((list, count) => {
      list[count.id] = count.threadRecentPostCount;
      this.cacheSet(this.recentPostCachePrefix, count.id, list[count.id]);
      return list;
    }, {});

    this.ids.forEach((id, index) => {
      if (cachedRecentPosts[index] !== null) result[id] = cachedRecentPosts[index];
    });

    return result;
  }

  private async getReadThreads(threads: Array<any>, userId: number) {
    if (userId == null || this.hasFlag(Thread.EXCLUDE_READ_THREADS)) return {};
    const threadIds = threads.map((thread) => thread.id);
    const threadIdentifier = knex.raw('??', ['ReadThreads.thread_id']);
    const lastSeenIdentifier = knex.raw('??', ['ReadThreads.last_seen']);

    const postsSince = knex('Posts')
      .count({ all: '*' })
      .where('Posts.thread_id', threadIdentifier)
      .where('Posts.created_at', '>', lastSeenIdentifier)
      .as('posts_since');

    const readThreads = await knex
      .select(
        'ReadThreads.thread_id',
        'ReadThreads.last_seen',
        postsSince,
        knex.raw(
          '(select id from Posts where Posts.thread_id = ReadThreads.thread_id and Posts.created_at > ReadThreads.last_seen order by Posts.created_at limit 1 ) as firstUnreadId'
        )
      )
      .from('ReadThreads')
      .where('ReadThreads.user_id', userId)
      .whereIn('ReadThreads.thread_id', threadIds);

    return readThreads.reduce((list, readThread) => {
      list[readThread.thread_id] = {
        lastSeen: readThread.last_seen,
        postsSince: readThread.posts_since,
        firstUnreadId: readThread.firstUnreadId,
      };
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.threadId,
      title: data.threadTitle,
      iconId: data.threadIconId,
      subforumId: data.threadSubforumId,
      createdAt: data.threadCreatedAt,
      updatedAt: data.threadUpdatedAt,
      deletedAt: data.threadDeletedAt,
      deleted: Boolean(data.threadDeletedAt != null),
      locked: Boolean(data.threadLocked),
      pinned: Boolean(data.threadPinned),
      subscribed: false,
      lastPost: {},
      backgroundUrl: data.threadBackgroundUrl,
      backgroundType: data.threadBackgroundType,
      user: data.threadUserId,
    };
  }

  async get() {
    // grab canonical data
    const cachedThreads = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedThreads);
    const uncachedThreads = await this.getThreads(uncachedIds);
    const threads = this.ids
      .map((id, index) => {
        if (
          cachedThreads[index] !== null &&
          (!this.hasFlag(Thread.HIDE_DELETED) || !cachedThreads[index].deleted)
        )
          return cachedThreads[index];
        if (!uncachedThreads[id]) return null;
        return Thread.format(uncachedThreads[id]);
      })
      .filter((thread) => thread != null);

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      threads.map(async (thread) => {
        await this.cacheSet(this.cachePrefix, thread.id, thread);
      });
    }

    // grab data from related caches
    const users = await this.getUsers(threads);
    const posts = await this.getLastPosts();
    const recentPostCounts = await this.getRecentPosts();
    const firstPostRatings = await this.getFirstPostRatings(threads);
    // eslint-disable-next-line @typescript-eslint/dot-notation
    const readThreads = await this.getReadThreads(threads, this.args['userId'] || null);
    const subforums = await this.getSubforums(threads);
    const tags = await this.getThreadTags(threads.map((thread) => thread.id));

    // merge related data in
    return threads
      .map((thread) => {
        const readThread = readThreads[thread.id] || null;
        thread.user = users[thread.user] || null;

        thread.lastPost = posts[thread.id] != null ? posts[thread.id].lastPost : {};
        thread.postCount = posts[thread.id] != null ? posts[thread.id].postCount : 0;
        thread.recentPostCount = recentPostCounts[thread.id] || 0;

        // holy shit, clean this mess up, too tightly coupled to rushed front-end implementation
        // write something that all clients can sanely consume
        thread.unreadPostCount = readThread != null ? readThread.postsSince : 0;
        thread.readThreadUnreadPosts = readThread != null ? readThread.postsSince : 0;
        thread.read = readThread != null;
        thread.hasRead = readThread != null;
        thread.hasSeenNoNewPosts = readThread != null && readThread.postsSince === 0;

        if (readThread) {
          thread.firstUnreadId = readThread.firstUnreadId;
        }

        if (firstPostRatings[thread.id]) {
          thread.firstPostTopRating = firstPostRatings[thread.id];
        }

        thread.subforum = subforums[thread.subforumId] || null;
        thread.tags = tags[thread.id] || [];

        return thread;
      })
      .filter(
        (thread) =>
          !(
            this.hasFlag(Thread.HIDE_NSFW) &&
            thread.tags &&
            thread.tags.some((tag) => tag[NSFW_TAG_ID] !== undefined)
          )
      );
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }

  async invalidatePosts() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.postCachePrefix, id);
      await this.cacheDrop(this.recentPostCachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const threadRetriever = new Thread(ids, []);
  await threadRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const threadRetriever = new Thread([id], []);
  await threadRetriever.invalidate();
};

export const invalidateObjectPosts = async (id: number) => {
  const threadRetriever = new Thread([id], []);
  await threadRetriever.invalidatePosts();
};

export const getFormattedObjects = async (ids: Array<number>, flags: Array<number> = []) => {
  const threadRetriever = new Thread(ids, flags);
  return threadRetriever.get();
};

export const getFormattedObject = async (id: number, flags: Array<number> = []) => {
  const threadRetriever = new Thread([id], flags);
  const threads = await threadRetriever.get();
  return threads[0];
};
