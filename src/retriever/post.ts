/* eslint-disable import/no-cycle */
import AbstractRetriever from './abstractRetriever';
import User from './user';
import Thread from './thread';
import PostRating from './postRating';
import Ban from './ban';
import knex from '../services/knex';
import { postsPerPage } from '../constants/pagination';
import convertSlateToBBCode from '../helpers/slateToBbCode';

export default class Post extends AbstractRetriever {
  private cachePrefix: string = 'post';

  public static INCLUDE_THREAD = 1;

  public static RETRIEVE_SHALLOW = 2;

  public static HIDE_DELETED = 3;

  public static HIDE_NSFW = 4;

  private static async getPosts(ids: Array<number>) {
    const results = await knex
      .from('Posts as p')
      .select(
        'p.id as postId',
        'p.content as postContent',
        'p.created_at as postCreatedAt',
        'p.updated_at as postUpdatedAt',
        'p.user_id as postUserId',
        'p.thread_id as postThreadId',
        'p.thread_post_number as postThreadPostNumber',
        'p.country_name as postCountryName',
        'p.app_name as postAppName'
      )
      .whereIn('p.id', ids);

    return results.reduce((list, thread) => {
      list[thread.postId] = thread;
      return list;
    }, {});
  }

  private async getUsers(posts: Array<any>) {
    if (this.hasFlag(Post.RETRIEVE_SHALLOW)) return {};
    const userIds = posts.map((post) => post.user);
    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private async getRatings(posts: Array<any>) {
    if (this.hasFlag(Post.RETRIEVE_SHALLOW)) return {};
    const postIds = posts.map((post) => post.id);
    const ratingRetriever = new PostRating(postIds, []);
    const rawRatings = await ratingRetriever.get();
    return postIds.reduce((list, postId, index) => {
      list[postId] = rawRatings[index];
      return list;
    }, {});
  }

  private async getThreads(posts: Array<any>) {
    if (!this.hasFlag(Post.INCLUDE_THREAD)) return [];
    const threadIds = posts
      .map((post) => post.thread)
      .filter((elem, index, self) => index === self.indexOf(elem));
    const flags = [Thread.INCLUDE_SUBFORUM, Thread.RETRIEVE_SHALLOW];
    if (this.hasFlag(Post.HIDE_DELETED)) {
      flags.push(Thread.HIDE_DELETED);
    }

    if (this.hasFlag(Post.HIDE_NSFW)) {
      flags.push(Thread.INCLUDE_TAGS);
      flags.push(Thread.HIDE_NSFW);
    }

    const threadRetriever = new Thread(threadIds, flags);
    const rawThreads = await threadRetriever.get();
    return rawThreads.reduce((list, thread) => {
      list[thread.id] = thread;
      return list;
    }, {});
  }

  private async getBans(posts: Array<any>) {
    if (this.hasFlag(Post.RETRIEVE_SHALLOW)) return {};
    const postIds = posts.map((post) => post.id);
    const postBans = await knex.from('Bans').select('id', 'post_id').whereIn('post_id', postIds);
    const banIds = postBans.map((ban) => ban.id);
    const banRetriever = new Ban(banIds, []);
    const bans = await banRetriever.get();
    const banIndex = bans.reduce((list, ban) => {
      list[ban.id] = ban;
      return list;
    }, {});
    return postBans.reduce((list, ban) => {
      if (typeof list[ban.post_id] === 'undefined') list[ban.post_id] = [];
      list[ban.post_id].push(banIndex[ban.id]);
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.postId,
      thread: data.postThreadId,
      page: Math.ceil(data.postThreadPostNumber / postsPerPage),
      content: convertSlateToBBCode(data.postContent),
      createdAt: data.postCreatedAt,
      updatedAt: data.postUpdatedAt,
      user: data.postUserId,
      ratings: [],
      bans: [],
      threadPostNumber: data.postThreadPostNumber,
      countryName: data.postCountryName,
      appName: data.postAppName,
    };
  }

  async get() {
    // grab canonical data
    const cachedPosts = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedPosts);
    const uncachedPosts = await Post.getPosts(uncachedIds);

    const posts = this.ids.map((id, index) => {
      if (cachedPosts[index] !== null) return cachedPosts[index];
      return Post.format(uncachedPosts[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      posts.map(async (post) => {
        await this.cacheSet(this.cachePrefix, post.id, post);
      });
    }

    // grab data from related caches
    const users = await this.getUsers(posts);
    const ratings = await this.getRatings(posts);
    const bans = await this.getBans(posts);
    const threads = await this.getThreads(posts);

    let filteredPosts = posts;

    if (this.hasFlag(Post.HIDE_DELETED) || this.hasFlag(Post.HIDE_NSFW)) {
      filteredPosts = posts.filter((post) => threads[post.thread] !== undefined);
    }

    // merge related data in
    return filteredPosts.map((post) => {
      const MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN = 300000;
      const postIsOldEnoughForRatings =
        new Date().getTime() - new Date(post.createdAt).getTime() >
        MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN;

      post.user = users[post.user] || post.user;
      post.ratings = (postIsOldEnoughForRatings && ratings[post.id]) || [];
      post.bans = bans[post.id] || [];
      post.thread = threads[post.thread] || post.thread;
      return post;
    });
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const postRetriever = new Post(ids, []);
  await postRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const postRetriever = new Post([id], []);
  await postRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>, flags = [Post.INCLUDE_THREAD]) => {
  const postRetriever = new Post(ids, flags);
  return postRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const postRetriever = new Post([id], []);
  const posts = await postRetriever.get();
  return posts[0];
};
