import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';

export default class Subforum extends AbstractRetriever {
  private cachePrefix: string = 'subforum';

  private static async getSubforums(ids: Array<number>) {
    const results = await knex
      .from('Subforums as sf')
      .select(
        'sf.id as subforumId',
        'sf.name as subforumName',
        'sf.icon_id as subforumIconId',
        'sf.created_at as subforumCreatedAt',
        'sf.updated_at as subforumUpdatedAt',
        'sf.description as subforumDescription'
      )
      .whereIn('sf.id', ids);

    return results.reduce((list, subforum) => {
      list[subforum.subforumId] = subforum;
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.subforumId,
      name: data.subforumName,
      iconId: data.subforumIconId,
      createdAt: data.subforumCreatedAt,
      updatedAt: data.subforumUpdatedAt,
      description: data.subforumDescription,
    };
  }

  async get() {
    // grab canonical data
    const cachedSubforums = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedSubforums);
    const uncachedSubforums = await Subforum.getSubforums(uncachedIds);
    const subforums = this.ids.map((id, index) => {
      if (cachedSubforums[index] !== null) return cachedSubforums[index];
      return Subforum.format(uncachedSubforums[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      subforums.map(async (subforum) => {
        await this.cacheSet(this.cachePrefix, subforum.id, subforum);
      });
    }

    return subforums;
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const subforumRetriever = new Subforum(ids, []);
  await subforumRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const subforumRetriever = new Subforum([id], []);
  await subforumRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>) => {
  const subforumRetriever = new Subforum(ids, []);
  return subforumRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const subforumRetriever = new Subforum([id], []);
  const subforums = await subforumRetriever.get();
  return subforums[0];
};
