import redis from "redis";
import { promisify } from "util";

const { REDIS_PORT, REDIS_HOST, REDIS_PASSWORD } = require("../../config/server");

const PORT = REDIS_PORT;
const HOST = REDIS_HOST;
const PASSWORD = REDIS_PASSWORD;

let args = {
  host: HOST,
  port: PORT,
  password: undefined
};

if (PASSWORD !== null) {
  args.password = PASSWORD;
}

const client = redis.createClient(args);

client.on("connect", () => {
  console.log("=========== Redis is connected ===========");
});

client.on("error", err => {
  console.log(`Redis Error: ${err}`);
});

client.getAsync = promisify(client.get).bind(client);
client.mgetAsync = promisify(client.mget).bind(client);

export default client;
