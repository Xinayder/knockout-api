import fs from 'fs';
import path from 'path';
import * as minio from 'minio';
import { NODE_ENV, S3_ENDPOINT, S3_BUCKET, S3_ACCESS_KEY, S3_SECRET_KEY } from "../../config/server";

const storeBufferRemotely = async (buffer, filePath, mimeType) => {
	const s3Client = new minio.Client({
    endPoint: S3_ENDPOINT,
    accessKey: S3_ACCESS_KEY,
    secretKey: S3_SECRET_KEY
  });
  const metaData = {
    'x-amz-acl': 'public-read',
    'Content-Type': mimeType
  };
  s3Client.putObject(S3_BUCKET, filePath, buffer, undefined, metaData, (e) => {
  	if (e) {
  		console.log('S3 upload failure: ' + filePath);
  	} else {
    	console.log('S3 upload complete: ' + filePath);
  	}
  });
}

const storeBufferLocally = async (buffer, filePath) => {
	const localPath = path.join(global.__basedir + '/static/' + filePath);
	fs.writeFile(localPath, buffer, (e) => {
		if (e) {
			console.log('Local upload failure: ' + localPath);
		} else {
			console.log('Local upload complete: ' + localPath);
		}
	});
}

const deleteFileRemotely = async (filePath) => {
  const s3Client = new minio.Client({
    endPoint: S3_ENDPOINT,
    accessKey: S3_ACCESS_KEY,
    secretKey: S3_SECRET_KEY
  });
  s3Client.removeObject(S3_BUCKET, filePath, (e) => {
    if (e) {
      console.log('S3 delete failure: ' + filePath);
    } else {
      console.log('S3 delete complete: ' + filePath)
    }
  });
}

const deleteFileLocally = async (filePath) => {
  const localPath = path.join(global.__basedir + '/static/' + filePath);
  fs.unlink(localPath, (e) => {
    if (e) {
      console.log('Local delete failure: ' + localPath);
    } else {
      console.log('Local delete complete: ' + localPath);
    }
  });
}

export default {
  storeBuffer: async (buffer, filePath, mimeType) => {
    if (NODE_ENV === 'production') {
      await storeBufferRemotely(buffer, filePath, mimeType);
    } else {
      await storeBufferLocally(buffer, filePath);
    }
  },
  deleteFile: async (filePath) => {
    if (NODE_ENV === 'production') {
      await deleteFileRemotely(filePath);
    } else {
      await deleteFileLocally(filePath);
    }
  }
}
