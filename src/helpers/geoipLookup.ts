import geoip from 'geoip-lite';
import countryLookup from 'country-code-lookup';

const ipLookup = (ip: string) => {
  return geoip.lookup(ip);
};

export const getCountryName = (geoData) => {
  try {
    const data = ipLookup(geoData);

    if (data) {
      return countryLookup.byIso(data.country).country
    }
  } catch (error) {
    return null;
  }
}