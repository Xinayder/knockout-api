import { Value } from 'slate';
import BBcode from 'slate-bbcode-serializer';

//                                         ,   ,
//                                         $,  $,     ,
//                                         "ss.$ss. .s'
//                                 ,     .ss$$$$$$$$$$s,
//                                 $. s$$$$$$$$$$$$$$`$$Ss
//                                 "$$$$$$$$$$$$$$$$$$o$$$       ,
//                                s$$$$$$$$$$$$$$$$$$$$$$$$s,  ,s
//                               s$$$$$$$$$"$$$$$$""""$$$$$$"$$$$$,
//                               s$$$$$$$$$$s""$$$$ssssss"$$$$$$$$"
//                              s$$$$$$$$$$'         `"""ss"$"$s""
//                              s$$$$$$$$$$,              `"""""$  .s$$s
//                              s$$$$$$$$$$$$s,...               `s$$'  `
//                          `ssss$$$$$$$$$$$$$$$$$$$$####s.     .$$"$.   , s-
//                            `""""$$$$$$$$$$$$$$$$$$$$#####$$$$$$"     $.$'
//                                  "$$$$$$$$$$$$$$$$$$$$$####s""     .$$$|
//                                   "$$$$$$$$$$$$$$$$$$$$$$$$##s    .$$" $
//                                    $$""$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"   `
//                                   $$"  "$"$$$$$$$$$$$$$$$$$$$$S""""'
//                              ,   ,"     '  $$$$$$$$$$$$$$$$####s
//                              $.          .s$$$$$$$$$$$$$$$$$####"
//                  ,           "$s.   ..ssS$$$$$$$$$$$$$$$$$$$####"
//                  $           .$$$S$$$$$$$$$$$$$$$$$$$$$$$$#####"
//                  Ss     ..sS$$$$$$$$$$$$$$$$$$$$$$$$$$$######""
//                   "$$sS$$$$$$$$$$$$$$$$$$$$$$$$$$$########"
//            ,      s$$$$$$$$$$$$$$$$$$$$$$$$#########""'
//            $    s$$$$$$$$$$$$$$$$$$$$$#######""'      s'         ,
//            $$..$$$$$$$$$$$$$$$$$$######"'       ....,$$....    ,$
//             "$$$$$$$$$$$$$$$######"' ,     .sS$$$$$$$$$$$$$$$$s$$
//               $$$$$$$$$$$$#####"     $, .s$$$$$$$$$$$$$$$$$$$$$$$$s.
//    )          $$$$$$$$$$$#####'      `$$$$$$$$$###########$$$$$$$$$$$.
//   ((          $$$$$$$$$$$#####       $$$$$$$$###"       "####$$$$$$$$$$
//   ) \         $$$$$$$$$$$$####.     $$$$$$###"             "###$$$$$$$$$   s'
//  (   )        $$$$$$$$$$$$$####.   $$$$$###"                ####$$$$$$$$s$$'
//  )  ( (       $$"$$$$$$$$$$$#####.$$$$$###' -Tua Xiong     .###$$$$$$$$$$"
//  (  )  )   _,$"   $$$$$$$$$$$$######.$$##'                .###$$$$$$$$$$
//  ) (  ( \.         "$$$$$$$$$$$$$#######,,,.          ..####$$$$$$$$$$$"
// (   )$ )  )        ,$$$$$$$$$$$$$$$$$$####################$$$$$$$$$$$"
// (   ($$  ( \     _sS"  `"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$S$$,
//  )  )$$$s ) )  .      .   `$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"'  `$$
//   (   $$$Ss/  .$,    .$,,s$$$$$$##S$$$$$$$$$$$$$$$$$$$$$$$$S""        '
//     \)_$$$$$$$$$$$$$$$$$$$$$$$##"  $$        `$$.        `$$.
//         `"S$$$$$$$$$$$$$$$$$#"      $          `$          `$
//             `"""""""""""""'         '           '           '
//
// Here be dragons. Thou art forewarned

const bbcode_isp = ['p', 'ul', 'ol', 'li', 'h1', 'h2']; // can display text on its own and cannot be nested inside p or isp
const bbcode_no_p_allowed = [
  ...bbcode_isp,
  'quote',
  'img',
  't',
  'video',
  'youtube',
  'twitter',
  'strawpoll'
]; // cannot be nested inside p or isp
const bbcode_must_be_in_p = ['b', 'i', 'u', 'sp', 'code', 'url']; // can be nested inside p or isp

const bbc = new BBcode(
  [
    {
      serialize(obj, children) {
        switch (obj.type) {
          case 'image':
            if (obj.data.get('isThumbnail')) {
              return (
                '[img thumbnail=""]' +
                (obj.data.src || (obj.data.get && obj.data.get('src'))) +
                '[/img]'
              );
            } else {
              return '[img]' + (obj.data.src || (obj.data.get && obj.data.get('src'))) + '[/img]';
            }
          case 'video':
            return '[video]' + (obj.data.src || (obj.data.get && obj.data.get('src'))) + '[/video]';
          case 'userquote':
            let postData = obj.data.postData || (obj.data.get && obj.data.get('postData'));
            let mentions = obj.data.mentionsUser || (obj.data.get && obj.data.get('mentionsUser'));
            console.log('postfdatad', postData, 'mentions', mentions);
            if (postData)
              return (
                '[quote username="' +
                postData.username +
                '" threadId="' +
                postData.threadId +
                '" threadPage="' +
                postData.threadPage +
                '" postId="' +
                postData.postId +
                '" mentionsUser="' +
                mentions +
                '"]' +
                children +
                '[/quote]'
              );
          case 'block-quote':
            return '[blockquote]' + children + '[/blockquote]';
          case 'youtube':
            return (
              '[youtube]' + (obj.data.src || (obj.data.get && obj.data.get('src'))) + '[/youtube]'
            );
          case 'twitter':
            return (
              '[twitter]' + (obj.data.src || (obj.data.get && obj.data.get('src'))) + '[/twitter]'
            );
          case 'strawpoll':
            return (
              '[strawpoll]' +
              (obj.data.src || (obj.data.get && obj.data.get('src'))) +
              '[/strawpoll]'
            );
          case 'bulleted-list':
            return '[ul]' + children + '[/ul]';
          case 'numbered-list':
            return '[ol]' + children + '[/ol]';
          case 'list-item':
            return '[li]' + children + '[/li]';
          case 'heading-one':
            return '[h1]' + children + '[/h1]';
          case 'heading-two':
            return '[h2]' + children + '[/h2]';
          case 'paragraph':
            //return children
            return '[p]' + children + '[/p]';
        }
      },
      deserialize(node, next) {
        switch (node.tag.toLowerCase()) {
          case 'img':
            return {
              object: 'block',
              type: 'image',
              data: { src: node.content[0], isThumbnail: false }
            };
          case 't':
            return {
              object: 'block',
              type: 'image',
              data: { src: node.content[0], isThumbnail: true }
            };
          case 'video':
            return { object: 'block', type: 'video', data: { src: node.content[0] } };
          case 'quote':
          case 'blockquote':
            if (node.attrs.postId) {
              let data = {
                threadId: node.attrs.threadId,
                threadPage: node.attrs.threadPage,
                postId: node.attrs.postId,
                username: node.attrs.username
              };
              for (let [k, v] of Object.entries(node.attrs)) {
                if (k == v) {
                  data.username = v;
                }
              }
              return {
                object: 'block',
                type: 'userquote',
                data: { postData: data, mentionsUser: node.attrs.mentionsUser },
                nodes: next(node.content)
              };
            } else {
              return { object: 'block', type: 'block-quote', data: {}, nodes: next(node.content) };
            }
          case 'youtube':
            return { object: 'block', type: 'youtube', data: { src: node.content[0] } };
          case 'twitter':
            return { object: 'block', type: 'twitter', data: { src: node.content[0] } };
          case 'strawpoll':
            return { object: 'block', type: 'strawpoll', data: { src: node.content[0] } };
          case 'ul':
            return { object: 'block', type: 'bulleted-list', data: {}, nodes: next(node.content) };
          case 'ol':
            return { object: 'block', type: 'numbered-list', data: {}, nodes: next(node.content) };
          case 'li':
            return { object: 'block', type: 'list-item', data: {}, nodes: next(node.content) };
          case 'h1':
            return { object: 'block', type: 'heading-one', data: {}, nodes: next(node.content) };
          case 'h2':
            return { object: 'block', type: 'heading-two', data: {}, nodes: next(node.content) };
          case 'p':
            return { object: 'block', type: 'paragraph', data: {}, nodes: next(node.content) };
        }
      }
    },
    {
      serialize(obj, children) {
        switch (obj.type) {
          case 'bold':
            return '[b]' + children + '[/b]';
          case 'italic':
            return '[i]' + children + '[/i]';
          case 'underlined':
            return '[u]' + children + '[/u]';
          case 'spoiler':
            return '[spoiler]' + children + '[/spoiler]';
          case 'code':
            return '[code]' + children + '[/code]';
        }
      },
      deserialize(node, next) {
        switch (node.tag.toLowerCase()) {
          case 'b':
            return { object: 'mark', type: 'bold', data: {}, nodes: next(node.content) };
          case 'i':
            return { object: 'mark', type: 'italic', data: {}, nodes: next(node.content) };
          case 'u':
            return { object: 'mark', type: 'underlined', data: {}, nodes: next(node.content) };
          case 'sp':
            return { object: 'mark', type: 'spoiler', data: {}, nodes: next(node.content) };
          case 'code':
            return { object: 'mark', type: 'code', data: {}, nodes: next(node.content) };
        }
      }
    },
    {
      serialize(obj, children) {
        switch (obj.type) {
          case 'link':
            console.log('link', JSON.stringify(obj.data), children);
            return (
              '[url href="' +
              (obj.data.href || (obj.data.get && obj.data.get('href'))) +
              '"]' +
              children +
              '[/url]'
            );
        }
      },
      deserialize(node, next) {
        switch (node.tag.toLowerCase()) {
          case 'url':
            console.log('bbcode url!!:', JSON.stringify(node.attrs));
            let data: any = { href: '' };
            for (let [k, v] of Object.entries(node.attrs)) {
              if (k == v && (v as string | string[]).length) {
                data.href = v;
                break;
              } else {
                data.href = k + '=' + v;
              }
            }
            if (data.href.length == 0) data.href = node.content[0];
            console.log('bbcode url:', JSON.stringify(data));
            return { object: 'inline', type: 'link', data: data, nodes: next(node.content) };
        }
      }
    }
  ],
  [
    ...bbcode_no_p_allowed,
    ...bbcode_must_be_in_p,
    ...bbcode_no_p_allowed.map(t => t.toUpperCase()),
    ...bbcode_must_be_in_p.map(t => t.toUpperCase())
  ]
);

class PostEditor {
  static getbbc() {
    return bbc;
  }

  static vegitate(obj) {
    if (obj.object == 'text') {
      let text = obj.text;
      let marks = obj.marks;
      delete obj.text;
      delete obj.marks;
      obj.leaves = [
        {
          object: 'leaf',
          text: text,
          marks: marks
        }
      ];
      return;
    }

    for (let [k, v] of Object.entries(obj)) {
      if (typeof v == 'object') {
        this.vegitate(v);
      }
    }
    return obj;
  }

  static alpha(c) {
    return (c.toLowerCase() >= 'a' && c.toLowerCase() <= 'z') || (c >= '0' && c <= '9');
  }

  static bare_text_allowed(tags) {
    let f = tags.filter(t => bbcode_no_p_allowed.includes(t));
    //this.log("baretextallowed", f, f.length)
    if (f.every(a => a == 'quote')) return false;
    return f.length != 0;
  }

  static log(...args) {
    return;
    let a = [...args];
    for (let i = 0; i < a.length; i++) {
      if (typeof a[i] == 'object') {
        a[i] = JSON.parse(JSON.stringify(a[i]));
      }
    }
    console.log(...a);
  }

  // herin i shall write my own parser to break tags for block elements and stuff
  static pp(text) {
    let cursor = 0;
    let tags = [];
    let fulltags = [];
    text = '[p]' + text;
    this.log('START OF FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    while (cursor < text.length) {
      if (text[cursor] == '[') {
        cursor++;
        let closing = false;
        if (text[cursor] == '/') {
          closing = true;
          cursor++;
        }
        let tagstart = cursor - 1;
        while (cursor < text.length && this.alpha(text[cursor])) {
          cursor++;
        }
        let tag = text.substring(tagstart + 1, cursor).toLowerCase();
        if (bbcode_no_p_allowed.includes(tag) || bbcode_must_be_in_p.includes(tag)) {
          let urlquote = false;
          if (!closing && tag == 'url' && text[cursor] == '=') {
            text = text.substring(0, cursor + 1) + '"' + text.substring(cursor + 1);
            cursor++;
            urlquote = true;
          }
          while (cursor < text.length && text[cursor] != ']' && text[cursor] != '[') {
            cursor++;
          }
          if (!closing && urlquote && tag == 'url') {
            text = text.substring(0, cursor) + '"' + text.substring(cursor);
            cursor++;
          }
          let tagend = cursor + 1;
          let fulltag = text.substring(tagstart, tagend);

          // check for tag incompatibilities and collapse if necessary
          if (!closing) {
            if (tags.includes('p') && bbcode_no_p_allowed.includes(tag)) {
              // need to break p!
              let broken_tags = [];
              let broken_fulltags = [];
              while (tags.includes('p')) {
                broken_tags.unshift(tags.pop());
                broken_fulltags.unshift(fulltags.pop());
              }
              tags.push(broken_tags);
              fulltags.push(broken_fulltags);
              this.log('tags push 1', tags);

              let ins = broken_tags.reduceRight((str, t) => str + '[/' + t + ']', '');

              this.log('breaking', ins);
              text = text.substring(0, tagstart) + ins + text.substring(tagstart);
              cursor += ins.length;
              tagstart += ins.length;
              tagend += ins.length;
            } else if (!this.bare_text_allowed(tags) && bbcode_must_be_in_p.includes(tag)) {
              // need a p!
              this.log('time to take a p', tag);
              text = text.substring(0, tagstart) + '[p]' + text.substring(tagstart);
              cursor += 3;
              tagstart += 3;
              tags.push('p');
              fulltags.push('[p]');
              this.log('tags push 2', tags);
              // i guess we'll just strip exta paragraphs at the end
            }
          } else if (tags.includes('p') && bbcode_no_p_allowed.includes(tag)) {
            // closing, close p
            let ins = '';
            while (tags.includes('p')) {
              ins += '[/' + tags.pop() + ']';
              fulltags.pop();
              this.log('tags pop 3', tags);
            }
            text = text.substring(0, tagstart - 1) + ins + text.substring(tagstart - 1);
            cursor += ins.length;
            tagstart += ins.length;
            tagend += ins.length;
            this.log(
              'closing before nop!!',
              text.substring(cursor - ins.length - 10, cursor),
              '>',
              text.substring(cursor, cursor + 10)
            );
          }

          // push/pop tags from stack and expand if necessary
          if (closing) {
            tags.pop();
            fulltags.pop();
            this.log('tags pop 4', tags);
            if (typeof tags[tags.length - 1] == 'object') {
              let top = tags.pop();
              let fulltop = fulltags.pop();
              this.log('tags pop 5', tags);
              // we can just add these to text and have them parsed as we get to them
              let ins = fulltop.join('');
              text = text.substring(0, tagend) + ins + text.substring(tagend);
              tagend += ins.length;
              tagstart += ins.length;
              this.log(
                'tags expanded',
                text.substring(cursor - ins.length - 10, cursor),
                '>',
                text.substring(cursor, cursor + 10)
              );
            }
          } else {
            tags.push(tag);
            fulltags.push(fulltag);
            this.log('tags push 6', tags);
          }
        } else {
          this.log('not a tag:', tag);
        }
      } else if (!this.bare_text_allowed(tags)) {
        this.log('bare text needs a p!');
        text = text.substring(0, cursor) + '[p]' + text.substring(cursor);
        cursor += 3;
        tags.push('p');
        fulltags.push('[p]');
        this.log('tags push 7', tags);
      }
      cursor++;
      //this.log("end of loop!!", text.substring(cursor - 5, cursor), '>', text.substring(cursor, cursor + 5))
    }
    while (tags.length > 0) {
      text += '[/' + tags.pop() + ']';
    }
    text = text.replace(/\[p\]\n/g, '[p]');
    text = text.replace(/\n\[\/p\]/g, '[/p]');
    return text.replace(/\[p\]\n?\[\/p\]/g, '');
  }

  static depp(text) {
    text = text.replace(/\[\/quote\]\n\[quote\]/g, '\n');
    text = text.replace(/\\\[/g, '[');
    text = text.replace(/\\\]/g, ']');
    text = text.replace(/\\\\/g, '\\');
    return text.replace(/\[\/?p\]/g, '');
  }

  render(text): string {
    return (
      bbc.serialize(
        Value.fromJSON(
          JSON.parse(text)
        ))
    );
  }
}

const peditor = new PostEditor();

const convertSlateToBBCode = postContent => {
  try {
    if (postContent.startsWith(`{"object":"value",`)) {
      const res= peditor.render(postContent).replace(/\[\/?p\]/gm, '')
      console.log(res)
      return res
    }

    return postContent;
  } catch (e) {
    console.log(e)
    return postContent;
  }
};

export default convertSlateToBBCode;