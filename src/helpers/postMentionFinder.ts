const { ShortcodeTree } = require('shortcode-tree');

const postMentionFinder = (content: string): number[] | false => {
  try {
    const mentions = [];
    const tree = ShortcodeTree.parse(content);
    tree.children.forEach((node) => {
      if (
        node.shortcode &&
        node.shortcode.name === 'quote' &&
        node.shortcode.properties.mentionsUser
      ) {
        mentions.push(Number(node.shortcode.properties.mentionsUser));
      }
    });
    return mentions;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export default postMentionFinder;
