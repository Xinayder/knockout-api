// @ts-check
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Users', 'deleted_at', {
      allowNull: true,
      type: Sequelize.DATE,
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Users', 'deleted_at');
  }
};
