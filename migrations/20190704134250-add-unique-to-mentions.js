'use strict';

const knex = require('../src/services/knex').knex;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await knex.schema.alterTable('Mentions', function(table) {
      table.unique(['mentioned_by', 'post_id', 'mentions_user']);
    });
  },

  down: async (queryInterface, Sequelize) => {
    await knex.schema.alterTable('Mentions', function(table) {
      table.unique(['mentioned_by', 'post_id', 'mentions_user']);
    });
  }
};
